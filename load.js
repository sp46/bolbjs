const Discord = require('discord.js');
const Client = new Discord.Client();

Client.on('ready', () => {
  console.log(`I'm BolbJS aka ${Client.user.tag}`);
});

Client.on('message', message => {
  if (message.content === 'ping') {
    message.reply('Pong!');
  }
});

/**
 * Error Handler...
 * 
 * Discord.js has a problem when the bot just times out after some random time, and this is the fix.
 * Error on error.
 * 
 * Seriously, Implement this by default, or at least in the Example bot!
 * (Which I use as a boilerplate)
 * 
 *  ⬇  ⬇  ⬇
 */
Client.on('error', console.error);​

Client.login('token');